module.exports =
  results: (results) ->
    listings = {}
    for r in results.results
      for pa in r.property_agents
        for ls in pa.listing_sources
          listings[ls.site] ||= []
          listings[ls.site].push {
            id: ls.listing_id
            source:
              hostname: ls.hostname
              pathname: ls.pathname
          }
    listings
  result: (result) ->
    listings = {}
    for pa in result.property_agents
      for ls in pa.listing_sources
        listings[ls.site] ||= []
        listings[ls.site].push {
          source:
            id: ls.id
            site: ls.site
            hostname: ls.hostname
            pathname: ls.pathname
        }
    listings
