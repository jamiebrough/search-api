moment = require('moment')

empty = (v) ->
  return v == null || v == undefined || v.length == 0

val = (v) ->
  if empty(v) then null else v

either = (v1, v2) ->
  if v1 or v2 then true else false

module.exports = class Query
  constructor: (@data) ->

  sort: () ->
    if @data.days
      'doc.date_updated'
    else
      switch @data.sort
        when 'price' then 'doc.price.amount_week'
        else 'doc.date_updated'

  date_from: () ->
    moment().subtract(@data.days-1, 'days').format('YYYY-MM-DD') if @data.days

  site: () ->
    val @data.site

  order: () ->
    d = @data.order
    if val d then d else 'desc'

  from: () ->
    d = @data.from
    if val d then d else 0

  size: () ->
    d = @data.size
    if val d then d else 100

  text_query: () ->
    val @data.text_query

  agent_query: () ->
    val @data.agent_query

  agents: () ->
    val(@data.agents)

  beds_min: () ->
    val @data.beds_min

  beds_max: () ->
    val @data.beds_max

  beds: () ->
    either @beds_min(), @beds_max()

  baths_min: () ->
    val @data.baths_min

  baths_max: () ->
    val @data.baths_max

  baths: () ->
    either @baths_min(), @baths_max()

  price_min: () ->
    val @data.price_min

  price_max: () ->
    val @data.price_max

  price: () ->
    either @price_min(), @price_max()

  features: () ->
    r = []
    for f in val(@data.features) or []
      r.push(f) if f not in ['furnished', 'unfurnished']
    r

  furnished: () ->
    r = []
    for f in val(@data.features) or []
      r.push(f) if f in ['furnished', 'unfurnished']
    r.push('furnished_u') if r.length > 0
    val(r)

  property_types: () ->
    val(@data.property_type)

  points: () ->
    a = []
    for l in @data.locations or []
      b = []
      b.push [p.lng, p.lat] for p in l.paths
      a.push(b)
    a

  agent_q: () ->
    query_string:
      fields: ['doc.property_agent.name', 'doc.property_agents.branch']
      query: @agent_query()

  text_q: () ->
    multi_match:
      fields: ['doc.address.post_code', 'doc.address_text', 'doc.text', 'doc.ocr', 'doc.listing_ids', '_id']
      query: @text_query()
      type: 'phrase_prefix'

  fulltext_q: () ->
    a = []
    a.push @agent_q() if @agent_query()
    a.push @text_q() if @text_query()
    a

  baths_q: () ->
    r =
      minimum_should_match: if @baths() then 1 else 0
      should: [
        { range: 'doc.accommodation.baths':
            gte: -1, lte: 0 }
      ]
    f = {}
    f.gte = @baths_min() if @baths_min()
    f.lte = @baths_max() if @baths_max()
    r.should.push range: 'doc.accommodation.baths': f
    r

  beds_q: () ->
    f = {}
    f.gte = @beds_min() if @beds_min()
    f.lte = @beds_max() if @beds_max()
    range: 'doc.accommodation.beds': f

  price_q: () ->
    f = {}
    f.gte = @price_min() if @price_min()
    f.lte = @price_max() if @price_max()
    range: 'doc.price.amount_week': f

  date_from_q: () ->
    f = {}
    f.gte= @date_from()
    range: 'doc.property_agents.listing_sources.date_updated': f

  must_q: () ->
    a = []
    a.push @beds_q() if @beds()
    a.push @price_q() if @price()
    a.push @date_from_q() if @date_from()
    a.push term: 'doc.features': f for f in @features()
    a.push terms: 'doc.features': @furnished() if @furnished()
    a.push terms: 'doc.accommodation.property_type': @property_types() if @property_types()
    a.push terms: 'doc.property_agents.name.raw': @agents() if @agents()
    a.push term: 'doc.archived': false
    a.push term: 'doc.source.site': @site() if @site()
    a

  polygon_q: () ->
    geo_polygon: 'doc.location': points: p for p in @points()

  sort_q: () ->
    r = {}
    r[@sort()] = order: @order()
    r

  aggs: () ->
    if @agent_query()
      agents:
        aggs:
          branches: terms:
            field: 'doc.property_agents.branch.raw', size: 9999
        terms:
          field: 'doc.property_agents.name.raw', size: 9999
    else
      {}

  query: () ->
    _source: excludes: ['doc.text', 'doc.ocr']
    aggs: @aggs()
    from: @from()
    size: @size()
    sort: @sort_q()
    query: bool:
      filter: bool:
        filter: bool: @baths_q()
        minimum_should_match: if @points().length == 0 then 0 else 1
        should: @polygon_q()
        must: @must_q()
      must: @fulltext_q()
