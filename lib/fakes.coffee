agent = (err, res) ->
  class Fake
    constructor: (@err, @res) ->
      @calls = []

    post: (@postURL) -> @
    get: (@getURL) -> @
    set: (accept, type) ->
      if accept == 'Accept'
        @contentType = type
      @
    auth: (@user, @pass) -> @
    send: (@data) -> @
    end: (@cb) ->
      @calls.push(
        get: @getURL,
        post: @postURL,
        user: @user,
        pass: @pass,
        data: @data,
      )
      @cb(@err, @res)

  new Fake(err, res)

module.exports = agent: agent
