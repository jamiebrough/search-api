test = require 'tape'
moment = require 'moment'

Query = require './query'

q = (k, v, k2) ->
  d = {}
  d[k]= v
  new Query(d)[k2 or k]()

request = () ->
  agent_query: '*'
  agents: ['foo', 'bar']
  sort: 'price'
  order: 'asc'
  baths_min: 3
  beds_min: 2
  days: 0
  features: [
    'balcony'
    'furnished'
  ]
  from: 0
  locations: [
    paths: [
      lat: 51.4989, lng: -0.1987
      lat: 51.4987, lng: -0.1995
    ]
    paths: [
      lat: 52.4989, lng: -1.1987
      lat: 52.4987, lng: -1.1995
    ]
  ]
  price_max: 500
  price_min: 100
  property_type: ['house']
  site: 'onthemarket'
  size: 50
  text_query: 'foo'

accom_ex = (o) ->
  [o, term: 'doc.archived': false]

test 'Query', (t) ->
  t.equal(q('from', 1), 1, 'from')
  t.equal(q('from', undefined), 0, 'from')

  t.equal(q('size', 1), 1, 'size')
  t.equal(q('size', undefined), 100, 'size')

  t.equal(q('days', 1, 'sort'), 'doc.date_updated', 'sort')
  t.equal(q('days', 0, 'sort'), 'doc.date_updated', 'sort')
  t.equal(q('sort', 'price'), 'doc.price.amount_week', 'sort')
  t.equal(q('sort', 'updated'), 'doc.date_updated', 'sort')

  t.equal(q('order', 'foo'), 'foo', 'order')
  t.equal(q('order', undefined), 'desc', 'order')

  t.equal(q('text_query', 'foo'), 'foo', 'text_query')
  t.equal(q('text_query', undefined), null, 'text_query')

  t.equal(q('agent_query', 'foo'), 'foo', 'agent_query')
  t.equal(q('agent_query', undefined), null, 'agent_query')

  t.deepEqual(q('agents', ['foo']), ['foo'], 'agents')
  t.deepEqual(q('agents', undefined), null, 'agents')

  t.equal(q('site', 'foo'), 'foo', 'site')
  t.equal(q('site', undefined), null, 'site')

  exp =
    aggs:
      branches: terms:
        field: 'doc.property_agents.branch.raw', size: 9999
    terms:
      field: 'doc.property_agents.name.raw', size: 9999

  t.deepEqual(q('agent_query', 'foo', 'aggs').agents, exp, 'aggs')
  t.equal(q('agent_query', undefined, 'aggs').agents, undefined, 'aggs')

  t.equal(q('beds_min', 1), 1, 'beds_min')
  t.equal(q('beds_min', undefined), null, 'beds_min')

  t.equal(q('beds_max', 1), 1, 'beds_max')
  t.equal(q('beds_max', undefined), null, 'beds_max')

  t.equal(q('beds_min', 1, 'beds'), true, 'beds')
  t.equal(q('beds_max', 1, 'beds'), true, 'beds')
  t.equal(new Query({}).beds(), false, 'beds')

  t.equal(q('baths_min', 1), 1, 'baths_min')
  t.equal(q('baths_min', undefined), null, 'baths_min')

  t.equal(q('baths_max', 1), 1, 'baths_max')
  t.equal(q('baths_max', undefined), null, 'baths_max')

  t.equal(q('baths_min', 1, 'baths'), true, 'baths')
  t.equal(q('baths_max', 1, 'baths'), true, 'baths')
  t.equal(new Query({}).baths(), false, 'baths')

  t.equal(q('price_min', 1), 1, 'price_min')
  t.equal(q('price_min', undefined), null, 'price_min')

  t.equal(q('price_max', 1), 1, 'price_max')
  t.equal(q('price_max', undefined), null, 'price_max')

  t.equal(q('price_min', 1, 'price'), true, 'price')
  t.equal(q('price_max', 1, 'price'), true, 'price')
  t.equal(new Query({}).price(), false, 'price')

  t.deepEqual(q('features', ['foo']), ['foo'], 'features')
  t.deepEqual(q('features', null), [], 'features')

  t.deepEqual(q('property_type', ['foo'], 'property_types'), ['foo'], 'property_types')
  t.deepEqual(q('property_type', undefined, 'property_types'), null, 'property_types')

  loc = [
    paths: [
      { lat: 51.4989, lng: -0.1987 }
      { lat: 51.4987, lng: -0.1995 }
    ]
  ]

  exp = [
    { geo_polygon: 'doc.location': points: [[ -0.1987, 51.4989 ], [ -0.1995, 51.4987 ]] }
  ]

  t.deepEqual(q('locations', loc, 'polygon_q'), exp, 'polygon_q')
  t.deepEqual(q('locations', null, 'points'), [], 'points')

  exp = [
    query_string:
      fields: ['doc.property_agent.name', 'doc.property_agents.branch']
      query: 'foo'
  ]
  t.deepEqual(q('agent_query', 'foo', 'fulltext_q'), exp, 'fulltext_q')

  date = moment().format('YYYY-MM-DD')
  exp =
    range:
      'doc.property_agents.listing_sources.date_updated':
        gte: date

  t.deepEqual(q('days', 1, 'date_from_q'), exp, 'from today')

  exp = [
    multi_match:
      fields: ['doc.address.post_code', 'doc.address_text', 'doc.text', 'doc.ocr', 'doc.listing_ids', '_id']
      query: 'foo'
      type: 'phrase_prefix'
  ]
  t.deepEqual(q('text_query', 'foo', 'fulltext_q'), exp, 'fulltext_q')

  t.deepEqual(q('baths_min', 1, 'baths_q').minimum_should_match, 1, 'baths_q[min]')

  t.deepEqual(q('baths_min', null, 'baths_q').minimum_should_match, 0, 'baths_q[min]')

  exp =
    range: 'doc.accommodation.baths':
      gte: 1

  t.deepEqual(q('baths_min', 1, 'baths_q').should[1], exp, 'baths_q')

  exp =
    range: 'doc.accommodation.baths':
      gte: -1
      lte: 0

  t.deepEqual(q('baths_min', 1, 'baths_q').should[0], exp, 'baths_q')

  exp = accom_ex(
    range: 'doc.accommodation.beds':
      gte: 1
  )
  t.deepEqual(q('beds_min', 1, 'must_q'), exp, 'must_q')

  exp = accom_ex(
    range: 'doc.accommodation.beds':
      lte: 1
  )
  t.deepEqual(q('beds_max', 1, 'must_q'), exp, 'must_q')

  exp = accom_ex(
    range: 'doc.price.amount_week':
      gte: 1
  )

  t.deepEqual(q('price_min', 1, 'must_q'), exp, 'must_q')

  exp = accom_ex(term: 'doc.features': 'foo')
  t.deepEqual(q('features', ['foo'], 'must_q'), exp, 'must_q')

  exp = accom_ex(terms: 'doc.accommodation.property_type': ['foo'])
  t.deepEqual(q('property_type', ['foo'], 'must_q'), exp, 'must_q')

  # test the full query is correctly structured
  exp =
    _source: excludes: ['doc.text', 'doc.ocr']
    aggs:
      agents:
        aggs:
          branches:
            terms:
              field: 'doc.property_agents.branch.raw'
              size: 9999
        terms:
          field: 'doc.property_agents.name.raw'
          size: 9999
    from: 0
    size: 50
    sort: 'doc.price.amount_week': order: 'asc'
    query:
      bool:
        filter:
          bool:
            filter:
              bool:
                minimum_should_match: 1
                should: [
                  { range: 'doc.accommodation.baths':
                      gte: -1
                      lte: 0 }
                  { range: 'doc.accommodation.baths':
                      gte: 3 }
                ]
            minimum_should_match: 1
            should: [{ geo_polygon: 'doc.location': points: [[-1.1995, 52.4987]] }]
            must: [
              { range: 'doc.accommodation.beds':
                  gte: 2 }
              { range: 'doc.price.amount_week':
                  gte: 100
                  lte: 500 }
              { term: 'doc.features': 'balcony' }
              { terms: 'doc.features': ['furnished', 'furnished_u']}
              { terms: 'doc.accommodation.property_type': ['house']}
              { terms: 'doc.property_agents.name.raw': ['foo', 'bar']}
              { term: 'doc.archived': false }
              { term: 'doc.source.site': 'onthemarket' }
            ]
        must: [
          { query_string:
              fields: ['doc.property_agent.name', 'doc.property_agents.branch']
              query: '*' }
          { multi_match:
              fields: ['doc.address.post_code', 'doc.address_text', 'doc.text', 'doc.ocr', 'doc.listing_ids', '_id']
              query: 'foo'
              type: 'phrase_prefix' }
        ]

  act = new Query(request()).query()

  actj = JSON.stringify(act)
  expj = JSON.stringify(exp)

  t.equal(actj, expj, 'full query')

  loc = [
    paths: [
      { lat: 51.4989, lng: -0.1987 }
      { lat: 51.4987, lng: -0.1995 }
    ]
  ]

  qs = [
    { sort: 'today'}, { days: 0 },
    { beds_min: 1 }, { beds_max: 2 }, { price_min: 1 }, { price_max: 2 },
    { features:  ['foo'] }, { from: 1 }, { property_type: ['house'] },
    { sites: ['bar'] }, { size: 2 }, { text_query: 'bar' }, { sort: 'price' },
    { order: 'desc' }, { agents: ['bar'] }, { agents_query: 'foo' }, { locations: loc }
  ]

  for h in qs
    for k, v of h
      try
        d = {}
        d[k]= v
        r = new Query(d).query
        t.pass("query compiled with #{k} => #{v}")
      catch e
        t.fail("query failed to compile with #{k} => #{v} - #{e}")

  t.plan(77)
  t.end()
