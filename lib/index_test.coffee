test = require 'tape'
agent = require 'superagent'
index = require './index'
fakes = require './fakes'
fixture = require './fixtures/response'

env =
  port: 0,
  apiHost: "https://foo.com",
  apiPort: "8080",
  esURL: "https://bar.com:9200",
  esIndex: "idx"

uri = (port, path) -> "http://127.0.0.1:#{port}/#{path}"

test 'GET /status', (t) ->
  fake_agent = fakes.agent(null, body: fixture)
  index env, fake_agent, (port, server) ->
    agent
    .get(uri(port, 'status'))
    .end (err, res) ->
      t.equal(res.body.status, 'green')
      t.plan(1)
      server.close()
      t.end()

test 'OPTIONS /properties/search/index', (t) ->
  fake_agent = fakes.agent(null, body: fixture)
  index env, fake_agent, (port, server) ->
    agent
    .post(uri(port, 'properties/search/index'))
    .send(baths_min: 2)
    .end (err, res) ->
      h = res.headers
      t.equal(h['access-control-allow-origin'], '*')
      t.equal(h['access-control-allow-headers'], 'Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization')
      t.equal(h['access-control-allow-methods'], 'GET, POST, OPTIONS')
      t.plan(3)
      server.close()
      t.end()

test 'POST /queue', (t) ->
  fake_agent = fakes.agent(null, body: fixture)
  index env, fake_agent, (port, server) ->
    agent
    .post(uri(port, 'queue'))
    .send(fixture.hits.hits[0]._source.doc)
    .end (err, res) ->
      t.equal(
        res.status
        202
      )
      t.equal(
        fake_agent.calls[0].post
        'https://foo.com:8080/queue/rightmove/listing'
      )
      t.equal(
        fake_agent.calls[0].data
        '[{"source":{"id":"64012004","site":"rightmove","hostname":"rightmove.co.uk","pathname":"/property-to-rent/property-64012004.html"}}]'
      )
      t.plan(3)
      server.close()
      t.end()

test 'POST /property/foo', (t) ->
  fake_agent = fakes.agent(null, body: fixture)
  index env, fake_agent, (port, server) ->
    agent
    .post(uri(port, 'property/foo'))
    .end (err, res) ->
      t.equal(
        fake_agent.calls[0].post
        'https://foo.com:8080/property/foo'
      )
      t.plan(1)
      server.close()
      t.end()

test 'GET /property/foo', (t) ->
  fake_agent = fakes.agent(null, body: fixture.hits.hits[0])
  index env, fake_agent, (port, server) ->
    agent
    .get(uri(port, 'property/foo'))
    .end (err, res) ->
      t.equal(
        fake_agent.calls[0].get
        'https://bar.com:9200/idx/property/foo'
      )
      t.plan(1)
      server.close()
      t.end()

test 'GET /listing/foo', (t) ->
  fake_agent = fakes.agent(null, body: fixture)
  index env, fake_agent, (port, server) ->
    agent
    .get(uri(port, 'listing/foo'))
    .end (err, res) ->
      t.equal(
        fake_agent.calls[0].get
        'https://foo.com:8080/listing/foo'
      )
      t.plan(1)
      server.close()
      t.end()

test 'POST /search', (t) ->
  fake_agent = fakes.agent(null, body: fixture)
  index env, fake_agent, (port, server) ->
    agent
    .post(uri(port, 'search'))
    .send(baths_min: 2)
    .end (err, res) ->
      t.equal(res.body.total, 151856)
      t.equal(
        fake_agent.calls[1].post
        'https://foo.com:8080/queue/rightmove/listing'
      )
      t.equal(
        fake_agent.calls[1].data
        '[{"id":"e9a12b2dffb67df20189de92405bef9f","source":{"hostname":"rightmove.co.uk","pathname":"/property-to-rent/property-64012004.html"}}]'
      )
      t.plan(3)
      server.close()
      t.end()
