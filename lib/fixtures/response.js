module.exports = {
    "_shards": {
        "failed": 0,
        "successful": 1,
        "total": 1
    },
    "hits": {
        "hits": [
            {
                "_id": "7f7cb2be-e7af-4bf1-99b6-64186e534b43",
                "_index": "properties-stage-sprint-1",
                "_score": 1.0,
                "_source": {
                    "doc": {
                        "accommodation": {
                            "baths": 0,
                            "beds": 2,
                            "floor_area": {
                                "unit": "",
                                "value": 0
                            },
                            "property_type": "flat"
                        },
                        "address": {
                            "country": "",
                            "flat_number": 0,
                            "post_code": "E16 1BZ",
                            "street_name": "",
                            "street_number": 0,
                            "town": ""
                        },
                        "address_text": "Seagull Lane, London, E16",
                        "archived": false,
                        "created_at": "2017-02-06 10:33:16",
                        "date_available": "2017-01-16",
                        "date_entered": "1970-01-01",
                        "date_updated": "2017-01-13",
                        "druid": "7f7cb2be-e7af-4bf1-99b6-64186e534b43",
                        "features": [
                            "balcony",
                            "parking",
                            "floor_u"
                        ],
                        "first_seen": "20170113T000000Z",
                        "listing_ids": [
                            "e9a12b2dffb67df20189de92405bef9f"
                        ],
                        "location": {
                            "lat": 51.508666,
                            "lon": 0.018107
                        },
                        "ocr_text": null,
                        "price": {
                            "amount": 420,
                            "amount_week": 420,
                            "currency": "GBP",
                            "frequency": "weekly"
                        },
                        "property_agents": [
                            {
                                "branch": "Canary Wharf - Lettings",
                                "branch_id": "b32c3992-215e-4809-ab8d-016e7034ea96",
                                "listing_sources": [
                                    {
                                        "commission": "",
                                        "date_entered": "1970-01-01",
                                        "date_updated": "2017-01-13",
                                        "delisted": false,
                                        "floorplans": null,
                                        "hostname": "rightmove.co.uk",
                                        "id": "64012004",
                                        "listing_id": "e9a12b2dffb67df20189de92405bef9f",
                                        "pathname": "/property-to-rent/property-64012004.html",
                                        "photos": [
                                            "http://media.rightmove.co.uk/dir/41k/40107/64012004/40107_CWL170058_L_IMG_01_0000_max_656x437.jpg",
                                            "http://media.rightmove.co.uk/dir/41k/40107/64012004/40107_CWL170058_L_IMG_02_0000_max_656x437.jpg",
                                            "http://media.rightmove.co.uk/dir/41k/40107/64012004/40107_CWL170058_L_IMG_03_0000_max_656x437.jpg",
                                            "http://media.rightmove.co.uk/dir/41k/40107/64012004/40107_CWL170058_L_IMG_04_0000_max_656x437.jpg"
                                        ],
                                        "price": {
                                            "amount": 420,
                                            "amount_week": 420,
                                            "currency": "GBP",
                                            "frequency": "weekly"
                                        },
                                        "site": "rightmove"
                                    }
                                ],
                                "name": "Atkinson Mcleod",
                                "phone": "020 8012 9166"
                            }
                        ],
                        "score": 82,
                        "taskId": "ef7b025e-487e-4aeb-9333-d3030fc18e91",
                        "text": [
                            "A spacious 2 double bedroom apartment in this modern development in Royal Victoria! There living space is open and airy with direct views to Canary Wharf from the private balcony. The open-plan kitchen is fully equipped with all appliances, two sizeable double bedrooms, master with esuite, and a ...",
                            "A spacious 2 double bedroom apartment in this modern development in Royal Victoria! There living space is open and airy with direct views to Canary Wharf from the private balcony. The open-plan kitchen is fully equipped with all appliances, two sizeable double bedrooms, master with esuite, and a contemporary guest bathroom. The property is offered furnished and resident's will further benefit from resident's parking. \n\nOur Ref: CWL170058",
                            " 2 Double Bedrooms",
                            " Spacious Apartment",
                            " Leisure Facilities",
                            " Concierge"
                        ],
                        "type": "property",
                        "updated_at": "2017-02-13 17:57:16"
                    }
                },
                "_type": "property"
            }
        ],
        "max_score": 1.0,
        "total": 151856
    },
    "timed_out": false,
    "took": 2
}
