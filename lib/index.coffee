express = require 'express'
bodyParser = require 'body-parser'

module.exports = (env, agent, cb) ->
  app = express()
  app.use bodyParser(limit: '5mb')
  app.use bodyParser.json()

  app.use (req, res, next) ->
    res.header 'Access-Control-Allow-Origin', '*'
    res.header 'Access-Control-Allow-Headers', 'Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization'
    res.header 'Access-Control-Allow-Methods', 'GET, POST, OPTIONS'
    if 'OPTIONS' == req.method then res.send 200 else next()

  client = require('./client')(agent, env.esURL, env.user, env.pass, env.esIndex)
  queue = require('./queue')(agent, env.apiHost, env.apiPort)

  app.get '/status', (req, res) ->
    res.setHeader 'Content-Type', 'application/json'
    res.send JSON.stringify(status: 'green')

  app.post '/queue', (req, res) ->
    queue.result req.body, (err) ->
      if err
        console.error(err)
        res.status(500)
        return res.send JSON.stringify(err)

      res.status(202).end()

  app.get '/listing/:id', (req, res) ->
    agent
    .get("#{env.apiHost}:#{env.apiPort}/listing/#{req.params.id}")
    .set('accept', 'application/json')
    .end (err, resp) ->
      if err
        console.error(err)
        return res.status(500).end()

      res.setHeader 'Content-Type', 'application/json'
      res.send JSON.stringify(resp.body)

  app.get '/property/:id', (req, res) ->
    client.get req.params.id, (err, r) ->
      if err
        console.error(err)
        res.status(500)
        return res.send JSON.stringify(err)

      res.setHeader 'Content-Type', 'application/json'
      res.send JSON.stringify(r)

  app.post '/property/:id', (req, res) ->
    agent
    .post("#{env.apiHost}:#{env.apiPort}/property/#{req.params.id}")
    .set('accept', 'application/json')
    .end (err, resp) ->
      if err
        console.error(err)
        return res.status(500).end()

      res.setHeader 'Content-Type', 'application/json'
      res.send JSON.stringify(resp.body)

  app.post '/search', (req, res) ->
    client.search req.body, (err, r) ->
      if err
        console.error(err)
        res.status(500)
        return res.send JSON.stringify(err)

      queue.results(r)

      res.setHeader 'Content-Type', 'application/json'
      res.send JSON.stringify(r)

  app.listen env.port, () ->
    cb(@.address().port, @)
