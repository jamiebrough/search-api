listings = require './listings'

module.exports = (agent, host, port) ->
  request = (parser, data, cb) ->
    sites = {}
    for site, ls of parser(data)
      agent
      .post("#{host}:#{port}/queue/#{site}/listing")
      .set('accept', 'application/json')
      .send(JSON.stringify(ls))
      .end (err) ->
        if err
          console.error(err)

        if cb
          cb(err)
  {
    results: (results, cb) ->
      request(listings.results, results, cb)
    result: (result, cb) ->
      request(listings.result, result, cb)
  }
