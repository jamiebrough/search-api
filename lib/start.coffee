require('coffee-script')

agent = require 'superagent'

index = require './index'

env =
  esURL: process.env.ES_HOSTNAME
  pass: process.env.ES_PASSWORD
  user: process.env.ES_USERNAME
  esIndex: process.env.ES_INDEX
  port: process.env.PORT
  apiHost: process.env.SCRAPE_API_SERVICE_HOST
  apiPort: process.env.SCRAPE_API_SERVICE_PORT

start = index(env, agent, (port) -> console.log("Listening on port #{port}"))
