Query = require './query'
response = require './response'

module.exports = (agent, url, user, pass, index) ->
  get: (id, cb) ->
    agent
    .get("#{url}/#{index}/property/#{id}")
    .set('Accept', 'application/json')
    .auth(user, pass)
    .end (err, res) ->
      return cb err if err

      if res.error
        cb res.status, res
      else
        t = new Date().getTime() - t
        cb null, res.body

  search: (params, cb) ->
    query = new Query(params).query()
    t = new Date().getTime()
    agent
    .post("#{url}/#{index}/_search")
    .set('Accept', 'application/json')
    .auth(user, pass)
    .send(query)
    .end (err, res) ->
      return cb err if err

      if res.error
        cb res.status, res
      else
        t = new Date().getTime() - t
        cb null, response(res.body, t, query)
