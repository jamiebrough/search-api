module.exports = (resp, total, query) ->
  agents = (r) ->
    if r.aggregations
      r.aggregations.agents.buckets
    else
      []

  response_time:
    upstream:
      total: total
      elasticsearch: resp.took
  results: (hit._source.doc for hit in resp.hits.hits)
  total: resp.hits.total
  agents: agents(resp)
  query: query
