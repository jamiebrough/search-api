test = require 'tape'
client = require './client'
fakes = require './fakes'
fixture = require './fixtures/response'

test 'client search', (t) ->
  agent = fakes.agent(null, body: fixture)
  req = client(agent, 'foo', 'bar', 'baz', 'idx').search
  req baths_min: 2, (err, res) ->
    t.equal(agent.contentType, 'application/json', 'content type')
    t.equal(agent.user, 'bar', 'user')
    t.equal(agent.pass, 'baz', 'pass')
    q = agent.data
    act = q.query.bool.filter.bool.filter.bool.should[1].range['doc.accommodation.baths'].gte
    t.equal(act, 2, 'query')
    t.ok(res.query, 'response includes es query')
    t.equal(agent.postURL, 'foo/idx/_search')
    t.plan(6)
    t.end()

test 'client get', (t) ->
  agent = fakes.agent(null, body: fixture.hits.hits[0])
  req = client(agent, 'foo', 'bar', 'baz', 'idx').get
  req "id1", (err, res) ->
    t.equal(agent.contentType, 'application/json', 'content type')
    t.equal(agent.user, 'bar', 'user')
    t.equal(agent.pass, 'baz', 'pass')
    t.equal(agent.getURL, 'foo/idx/property/id1')
    t.equal(res._type, 'property')
    t.plan(5)
    t.end()
