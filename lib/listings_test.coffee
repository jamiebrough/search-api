test = require 'tape'

listings = require './listings'

results = () ->
  results:
    [
      {
        property_agents: [
          {
            listing_sources: [
              { hostname: 'hostname1', pathname: '/pathname1', listing_id: '1', site: 'foo' }
              { hostname: 'hostname2', pathname: '/pathname2', listing_id: '2', site: 'bar' }
            ]
          }
          {
            listing_sources: [
              { hostname: 'hostname3', pathname: '/pathname3', listing_id: '3', site: 'foo'}
            ]
          }
        ]
      }
      {
        property_agents: [
          {
            listing_sources: [
              { hostname: 'hostname4', pathname: '/pathname4', listing_id: '4', site: 'bar' }
            ]
          }
        ]
      }
    ]

test 'listings', (t) ->
  expected = {
    foo: [
      { id: '1', source: { hostname: 'hostname1', pathname: '/pathname1' }}
      { id: '3', source: { hostname: 'hostname3', pathname: '/pathname3' }}
    ]
    bar: [
      { id: '2', source: { hostname: 'hostname2', pathname: '/pathname2' }}
      { id: '4', source: { hostname: 'hostname4', pathname: '/pathname4' }}
    ]
  }

  t.deepEqual(listings.results(results()), expected)
  t.plan(1)
  t.end()
